package libby.stuart;

public class Camisa {
    private double precio;
    private Talla talla_camisa;
    public Camisa(Talla t, double p){
        talla_camisa = t;
        precio = p;
    }

    public Camisa(){

    }
    public String toString(){
        return this.talla_camisa.toString()+", "+this.talla_camisa.getAbreviatura()+", " +precio;
    }
}
