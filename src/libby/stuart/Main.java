package libby.stuart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class Main {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    public static void main(String[] args) throws IOException {
	Camisa c = new Camisa(Talla.GRANDE,5000);
	out.println(c.toString());
	out.println("Digite la talla: PEQUENNO, MEDIANO, GRANDE");
	String talla = in.readLine();
	Talla t = Enum.valueOf(Talla.class,talla);
	c = new Camisa(t, 10000);
	out.println(c.toString());
    }
}
