package libby.stuart;

public enum Talla {
    PEQUENNO("P"),MEDIANO("M"),GRANDE("G");//invocando al constructor
    private String abreviatura;
    private Talla(String abreviatura){
        this.abreviatura = abreviatura;
    }

    public String getAbreviatura(){
        return abreviatura;
    }
}
